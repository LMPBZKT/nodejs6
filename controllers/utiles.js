const { response , request } = require('express')
const DatosUtiles = require('../data/data-util')
const utilEscolar = require('../models/utilEscolares')

const Util = new DatosUtiles()

const utilGet = (req, res = response) => {

  let datos = Util.getUtiles()

  res.json({
    datos
  })
}


const utilPost = (req = request, res = response) => {
  const { material,precioUnidad, moneda} = req.body;

  let precio1 = precioUnidad['c-1'];
  let precio2 = precioUnidad['c-2'];

  let utilNuevo = new utilEscolar(material, precioUnidad, moneda, precio1,precio2)

  Util.anadirUtil(utilNuevo)

  res.json({
    utilNuevo,
    msg: 'Util anadido'
  })
}

const utilPut = (req = request, res = response) => {
  const { id } = req.params
  const { material, precioUnidad,moneda } = req.body;
  let precio1 = precioUnidad['c-1'];
  let precio2 = precioUnidad['c-2'];

  const index = Util.data.findIndex(object => object.id_compra === id)

  if (index !== -1) {
    Util.editarUtilEs({ material, precioUnidad, moneda, precio1, precio2 }, index)
    res.json({
      msg: 'Util modificado'
    })
  } else {
    res.json({
      msg: 'Util no encontrado'
    })
  }
}

const utilDelete = (req = request, res = response) => {
  const {id} = req.params
  const index = Util.data.findIndex(object => object.id_compra === id)
  if (index !== -1) {
    Util.eliminarUtilEs(index)
    res.json({
      msg: 'Util eliminado'
    })
  } else {
    res.json({
      msg: 'Util no encontrado'
    })
  }
}

module.exports = {
  utilGet,
  utilPost,
  utilPut,
  utilDelete
}