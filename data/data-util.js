const fs = require('fs')
const UtilRoute = './data/util.json'

class DatosUtiles {
  constructor() {
    this.data = []
    this.leer = fs.readFileSync
    this.escribir = fs.writeFileSync
    this.cargardatos()
  }


  cargardatos = () => {
    if (!fs.existsSync(UtilRoute) || this.data.length !== 0) {
      return null
    } else {
      let rawdata = this.leer(UtilRoute)
      this.data = JSON.parse(rawdata)
    }
  }


  guardar = () => {
    let data = JSON.stringify(this.data, null, 2);
    this.escribir(UtilRoute, data)
  }


  getUtiles = () => {
    return this.data
  }


  anadirUtil = (util) => {
    this.data.push(util)
    this.guardar()
  }


  editarUtilEs = ( index) => {
    this.data[index].material = material
    this.data[index].precioUnidad = precioUnidad
    this.data[index].moneda = moneda
    this.data[index].precioPagar = precio1 + precio2
    this.guardar()
  }

  eliminarUtilEs = (id) => {
    this.data.splice(id, 1)
    this.guardar()
  }
}

module.exports = DatosUtiles
