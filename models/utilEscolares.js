const { v4: uuidv4 } = require('uuid');

class utilEscolar {
  constructor(material, precioUnidad,moneda, precio1, precio2) {
    this.id_compra = uuidv4();
    this.material = material;
    this.precioUnidad = precioUnidad;
    this.moneda = moneda;
    this.precioPagar = (precio1 + precio2) ;
  }
}

module.exports = utilEscolar